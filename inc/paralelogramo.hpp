#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"

class Paralelogramo : public FormaGeometrica{
    private:
        float baseB;
    public:
        Paralelogramo();
        Paralelogramo(string tipo, float base, float altura, float baseB);
        ~Paralelogramo();

        float calcula_area();
        float calcula_perimetro();

        void set_baseB(float baseB);
        float get_baseB();
};

#endif