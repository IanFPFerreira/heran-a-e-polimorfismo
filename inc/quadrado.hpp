#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"

class Quadrado : public FormaGeometrica{
    public:
        Quadrado();
        Quadrado(string tipo, float base, float altura);
        ~Quadrado();
};

#endif