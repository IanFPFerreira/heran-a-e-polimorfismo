#include "triangulo.hpp"
#include <iostream>

Triangulo::Triangulo(){
    set_tipo("Triângulo Equilátero");
    set_base(12.0);
    set_altura(8.0);
}
Triangulo::~Triangulo(){
    
}
Triangulo::Triangulo(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
float Triangulo::calcula_area(){
    return (get_base() * get_altura()) / 2;
}
float Triangulo::calcula_perimetro(){
    return 3*get_base();
}