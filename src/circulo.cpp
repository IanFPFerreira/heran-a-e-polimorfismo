#include "circulo.hpp"
#include <iostream>
#define PI           3.1415

Circulo::Circulo(){
    set_tipo("Círculo");
    raio = 5.0;
}
Circulo::~Circulo(){
   
}
Circulo::Circulo(string tipo, float raio){
    this->raio = raio;
    set_tipo(tipo);
}
float Circulo::calcula_area(){
    return raio * raio * PI;
}
float Circulo::calcula_perimetro(){
    return 2 * PI * raio;
}