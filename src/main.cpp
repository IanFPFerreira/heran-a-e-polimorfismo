#include <iostream>
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"
#include "formageometrica.hpp"
#include <vector>

int main(int argc, char const *argv[])
{
    FormaGeometrica forma1;
    Triangulo triangulo;
    Quadrado quadrado;
    Circulo circulo;
    Paralelogramo paralelogramo;
    Pentagono pentagono;
    Hexagono hexagono;

    vector <FormaGeometrica *> lista_de_formas;

    lista_de_formas.push_back(new FormaGeometrica(1.0, 2.0));
    lista_de_formas.push_back(new Triangulo("Triângulo Equilátero", 12.0, 8.0));
    lista_de_formas.push_back(new Quadrado("Quadrado", 2.0, 2.0));
    lista_de_formas.push_back(new Circulo("Círculo", 5.0));
    lista_de_formas.push_back(new Paralelogramo("Paralelogramo", 7.0, 5.0, 3.0));
    lista_de_formas.push_back(new Pentagono("Pentágono", 6.0, 4.0));
    lista_de_formas.push_back(new Hexagono("Hexágono", 4.0));

    for(FormaGeometrica * f: lista_de_formas){
        cout << "Tipo: " << f->get_tipo() << endl;
        cout << "Área: " << f->calcula_area() << endl;
        cout << "Perímetro: " << f->calcula_perimetro() << endl << endl;
    }

    return 0;
}