#include "paralelogramo.hpp"
#include <iostream>

Paralelogramo::Paralelogramo(){
    set_tipo("Paralelogramo");
    set_base(7.0);
    baseB = 5.0;
    set_altura(3.0);
}
Paralelogramo::~Paralelogramo(){
    
}
Paralelogramo::Paralelogramo(string tipo, float base, float baseB, float altura){
    set_tipo(tipo);
    set_base(base);
    this->baseB = baseB;
    set_altura(altura);
}
float Paralelogramo::calcula_area(){
    return 2*get_base() + 2*baseB;
}
float Paralelogramo::calcula_perimetro(){
    return get_base() * get_altura();
}