#include "pentagono.hpp"
#include <iostream>

Pentagono::Pentagono(){
    set_tipo("Pentágono");
    set_base(6.0);
    set_altura(4.0);
}
Pentagono::~Pentagono(){
    
}
Pentagono::Pentagono(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
float Pentagono::calcula_area(){
    return (5*get_base() * get_altura()) / 2;
}
float Pentagono::calcula_perimetro(){
    return 5*get_base();
}